﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// Holds all the static variables that are reused on each playthrough
    /// </summary>
    public static class StaticContent
    {
        const int tileSize = 16;

        static List<string> tempskel = new List<string>()
        {
            "Skeleton Walk",
            "Skeleton Attack",
            "Skeleton Dead"
        };

        static Dictionary<string, int> tempskelani = new Dictionary<string, int>()
        {
            { "Skeleton Walk", 13 },
            { "Skeleton Attack", 18 },
            { "Skeleton Dead", 15 }
        };

        /// <summary>
        /// Static list of enemies
        /// </summary>
        public static List<Enemy> StaticListOfEnemies = new List<Enemy>();
        /// <summary>
        /// Static list of notes
        /// </summary>
        public static List<Note> StaticListOfNotes = new List<Note>();
        /// <summary>
        /// Static list of NPC
        /// </summary>
        public static List<Siblings> StaticListOfSiblings = new List<Siblings>();

        public static EasterEgg ChristmasTree;

        /// <summary>
        /// Repopulates all the static lists
        /// </summary>
        public static void RepopulateStaticLists ()
        {
            ChristmasTree = new EasterEgg("Juletr", new Transform(new Vector2(8 * 16, 3 * 16), 0f));

            StaticListOfSiblings = new List<Siblings>
            {
                new Siblings("Zarfur", new Transform(new Vector2(7*tileSize, 4*tileSize), 0f)),
                new Siblings("Tsima", new Transform(new Vector2(6*tileSize, 4*tileSize), 0f))
            };

            StaticListOfEnemies = new List<Enemy>()
            {
                // Level 1 creeps
                new Enemy(tempskel, tempskelani, 24, new Vector2(23 * tileSize, 62 * tileSize), new Vector2(30 * tileSize, 62 * tileSize), new Transform(new Vector2(23 * tileSize, 62 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(49 * tileSize, 65 * tileSize), new Vector2(63 * tileSize, 65 * tileSize), new Transform(new Vector2(61 * tileSize, 65 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(71 * tileSize, 65 * tileSize), new Vector2(85 * tileSize, 65 * tileSize), new Transform(new Vector2(73 * tileSize, 65 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(8 * tileSize, 51 * tileSize), new Vector2(50 * tileSize, 51 * tileSize), new Transform(new Vector2(9 * tileSize, 51 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(8 * tileSize, 51 * tileSize), new Vector2(50 * tileSize, 51 * tileSize), new Transform(new Vector2(48 * tileSize, 51 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(13 * tileSize, 43 * tileSize), new Vector2(26 * tileSize, 43 * tileSize), new Transform(new Vector2(16 * tileSize, 43 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(95 * tileSize, 43 * tileSize), new Vector2(117 * tileSize, 43 * tileSize), new Transform(new Vector2(98 * tileSize, 43 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(51 * tileSize, 36 * tileSize), new Vector2(60 * tileSize, 36 * tileSize), new Transform(new Vector2(51 * tileSize, 36 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(62 * tileSize, 43 * tileSize), new Vector2(90 * tileSize, 43 * tileSize), new Transform(new Vector2(77 * tileSize, 43 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(10 * tileSize, 31 * tileSize), new Vector2(37 * tileSize, 31 * tileSize), new Transform(new Vector2(11 * tileSize, 31 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(26 * tileSize, 31 * tileSize), new Vector2(60 * tileSize, 31 * tileSize), new Transform(new Vector2(55 * tileSize, 31 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(11 * tileSize, 19 * tileSize), new Vector2(27 * tileSize, 19 * tileSize), new Transform(new Vector2(26 * tileSize, 19 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(17 * tileSize, 6 * tileSize), new Vector2(22 * tileSize, 6 * tileSize), new Transform(new Vector2(19 * tileSize, 6 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(36 * tileSize, 19 * tileSize), new Vector2(52 * tileSize, 19 * tileSize), new Transform(new Vector2(47 * tileSize, 19 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(54 * tileSize, 15 * tileSize), new Vector2(78 * tileSize, 15 * tileSize), new Transform(new Vector2(77 * tileSize, 15 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(73 * tileSize, 31 * tileSize), new Vector2(100 * tileSize, 31 * tileSize), new Transform(new Vector2(99 * tileSize, 31 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(94 * tileSize, 35 * tileSize), new Vector2(117 * tileSize, 35 * tileSize), new Transform(new Vector2(100 * tileSize, 35 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(102 * tileSize, 2 * tileSize), new Vector2(117 * tileSize, 2 * tileSize), new Transform(new Vector2(105 * tileSize, 2 * tileSize), 0), 0),
                new Enemy(tempskel, tempskelani, 24, new Vector2(59 * tileSize, 54 * tileSize), new Vector2(92 * tileSize, 54 * tileSize), new Transform(new Vector2(76 * tileSize, 54 * tileSize), 0), 0),

                // Level 2 fucks
                new Enemy(tempskel, tempskelani, 24, new Vector2(100 * tileSize, 2 * tileSize), new Vector2(105 * tileSize, 2 * tileSize), new Transform(new Vector2(100 * tileSize,  2 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(98 * tileSize, 5 * tileSize), new Vector2(105 * tileSize, 5 * tileSize), new Transform(new Vector2(104 * tileSize,  5 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(98 * tileSize, 5 * tileSize), new Vector2(105 * tileSize, 5 * tileSize), new Transform(new Vector2(98 * tileSize,  5 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(109 * tileSize, 11 * tileSize), new Vector2(114 * tileSize, 11 * tileSize), new Transform(new Vector2(110 * tileSize,  11 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(93 * tileSize, 10 * tileSize), new Vector2(102 * tileSize, 10 * tileSize), new Transform(new Vector2(99 * tileSize,  10 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(96 * tileSize, 31 * tileSize), new Vector2(115 * tileSize, 31 * tileSize), new Transform(new Vector2(101 * tileSize,  31 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(108 * tileSize, 17 * tileSize), new Vector2(111 * tileSize, 17 * tileSize), new Transform(new Vector2(109 * tileSize,  17 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(70 * tileSize, 10 * tileSize), new Vector2(86 * tileSize, 10 * tileSize), new Transform(new Vector2(73 * tileSize,  10 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(71 * tileSize, 6 * tileSize), new Vector2(74 * tileSize, 6 * tileSize), new Transform(new Vector2(71 * tileSize,  6 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(81 * tileSize, 2 * tileSize), new Vector2(85 * tileSize, 2 * tileSize), new Transform(new Vector2(83 * tileSize,  2 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(81 * tileSize, 16 * tileSize), new Vector2(84 * tileSize, 16 * tileSize), new Transform(new Vector2(82 * tileSize,  16 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(81 * tileSize, 22 * tileSize), new Vector2(84 * tileSize, 22 * tileSize), new Transform(new Vector2(83 * tileSize,  22 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(73 * tileSize, 40 * tileSize), new Vector2(76 * tileSize, 40 * tileSize), new Transform(new Vector2(73 * tileSize,  40 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(83 * tileSize, 32 * tileSize), new Vector2(85 * tileSize, 32 * tileSize), new Transform(new Vector2(84 * tileSize,  32 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(91 * tileSize, 34 * tileSize), new Vector2(96 * tileSize, 34 * tileSize), new Transform(new Vector2(91 * tileSize,  34 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(98 * tileSize, 37 * tileSize), new Vector2(101 * tileSize, 37 * tileSize), new Transform(new Vector2(100 * tileSize,  37 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(106 * tileSize, 49 * tileSize), new Vector2(108 * tileSize, 49 * tileSize), new Transform(new Vector2(106 * tileSize,  49 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(111 * tileSize, 52 * tileSize), new Vector2(116 * tileSize, 52 * tileSize), new Transform(new Vector2(112 * tileSize,  52 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(106 * tileSize, 55 * tileSize), new Vector2(111 * tileSize, 55 * tileSize), new Transform(new Vector2(110 * tileSize,  55 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(89 * tileSize, 52 * tileSize), new Vector2(94 * tileSize, 52 * tileSize), new Transform(new Vector2(91 * tileSize,  52 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(74 * tileSize, 58 * tileSize), new Vector2(80 * tileSize, 58 * tileSize), new Transform(new Vector2(79 * tileSize,  58 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(95 * tileSize, 60 * tileSize), new Vector2(113 * tileSize, 60 * tileSize), new Transform(new Vector2(108 * tileSize,  60 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(53 * tileSize, 52 * tileSize), new Vector2(56 * tileSize, 52 * tileSize), new Transform(new Vector2(53 * tileSize,  52 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(55 * tileSize, 49 * tileSize), new Vector2(60 * tileSize, 49 * tileSize), new Transform(new Vector2(57 * tileSize,  49 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(56 * tileSize, 46 * tileSize), new Vector2(60 * tileSize, 46 * tileSize), new Transform(new Vector2(58 * tileSize,  46 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(55 * tileSize, 43 * tileSize), new Vector2(60 * tileSize, 43 * tileSize), new Transform(new Vector2(56 * tileSize,  43 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(45 * tileSize, 40 * tileSize), new Vector2(47 * tileSize, 40 * tileSize), new Transform(new Vector2(42 * tileSize,  40 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(32 * tileSize, 43 * tileSize), new Vector2(37 * tileSize, 43 * tileSize), new Transform(new Vector2(34 * tileSize,  43 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(32 * tileSize, 46 * tileSize), new Vector2(36 * tileSize, 46 * tileSize), new Transform(new Vector2(35 * tileSize,  46 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(32 * tileSize, 49 * tileSize), new Vector2(37 * tileSize, 49 * tileSize), new Transform(new Vector2(32 * tileSize,  49 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(36 * tileSize, 52 * tileSize), new Vector2(39 * tileSize, 52 * tileSize), new Transform(new Vector2(38 * tileSize,  52 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(28 * tileSize, 50 * tileSize), new Vector2(30 * tileSize, 50 * tileSize), new Transform(new Vector2(28 * tileSize,  50 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(5 * tileSize, 41 * tileSize), new Vector2(9 * tileSize, 41 * tileSize), new Transform(new Vector2(8 * tileSize,  41 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(5 * tileSize, 52 * tileSize), new Vector2(8 * tileSize, 52 * tileSize), new Transform(new Vector2(7 * tileSize,  52 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(9 * tileSize, 55 * tileSize), new Vector2(12 * tileSize, 55 * tileSize), new Transform(new Vector2(10 * tileSize,  55 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(48 * tileSize, 35 * tileSize), new Vector2(51 * tileSize, 35 * tileSize), new Transform(new Vector2(49 * tileSize,  35 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(55 * tileSize, 32 * tileSize), new Vector2(57 * tileSize, 32 * tileSize), new Transform(new Vector2(55 * tileSize,  32 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(44 * tileSize, 32 * tileSize), new Vector2(46 * tileSize, 32 * tileSize), new Transform(new Vector2(45 * tileSize,  32 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(44 * tileSize, 26 * tileSize), new Vector2(46 * tileSize, 26 * tileSize), new Transform(new Vector2(44 * tileSize,  26 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(56 * tileSize, 22 * tileSize), new Vector2(58 * tileSize, 22 * tileSize), new Transform(new Vector2(56 * tileSize,  22 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(46 * tileSize, 17 * tileSize), new Vector2(48 * tileSize, 17 * tileSize), new Transform(new Vector2(47 * tileSize,  17 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(60 * tileSize, 7 * tileSize), new Vector2(64 * tileSize, 7 * tileSize), new Transform(new Vector2(62 * tileSize,  7 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(48 * tileSize, 6 * tileSize), new Vector2(54 * tileSize, 6 * tileSize), new Transform(new Vector2(53 * tileSize,  6 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(33 * tileSize, 13 * tileSize), new Vector2(37 * tileSize, 13 * tileSize), new Transform(new Vector2(35 * tileSize,  13 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(24 * tileSize, 27 * tileSize), new Vector2(27 * tileSize, 27 * tileSize), new Transform(new Vector2(24 * tileSize,  27 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(13 * tileSize, 25 * tileSize), new Vector2(15 * tileSize, 25 * tileSize), new Transform(new Vector2(14 * tileSize,  25 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(6 * tileSize, 18 * tileSize), new Vector2(9 * tileSize, 18 * tileSize), new Transform(new Vector2(8 * tileSize,  18 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(13 * tileSize, 21 * tileSize), new Vector2(16 * tileSize, 21 * tileSize), new Transform(new Vector2(13 * tileSize,  21 * tileSize), 0), 2),
                new Enemy(tempskel, tempskelani, 24, new Vector2(5 * tileSize, 14 * tileSize), new Vector2(8 * tileSize, 14 * tileSize), new Transform(new Vector2(5 * tileSize,  14 * tileSize), 0), 2),
            };

            StaticListOfNotes = new List<Note>()
            {
                // LEVEL 1
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 9", new Transform(new Vector2(26 * tileSize, 37 * tileSize), 0f), 0),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 10", new Transform(new Vector2(64 * tileSize, 37 * tileSize), 0f), 0),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 4", new Transform(new Vector2(78 * tileSize, 5 * tileSize), 0f), 0),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 8", new Transform(new Vector2(27 * tileSize, 5 * tileSize), 0f), 0),

                // LEVEL 1 SOM ÅBNER LEVEL 2
                new Note(true, NoteType.Zarfur, "Notes/Zarfur 2", new Transform(new Vector2(117 * tileSize, 3 * tileSize), 0f), 0),


                // LEVEL 2
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 3", new Transform(new Vector2(70 * tileSize, 11 * tileSize), 0f), 2),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 5", new Transform(new Vector2(98 * tileSize, 25 * tileSize), 0f), 2),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 6", new Transform(new Vector2(90 * tileSize, 45 * tileSize), 0f), 2),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 1", new Transform(new Vector2(32 * tileSize, 44 * tileSize), 0f), 2),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 7", new Transform(new Vector2(13 * tileSize, 13 * tileSize), 0f), 2),
                new Note(false, NoteType.Zarfur, "Notes/Zarfur 11", new Transform(new Vector2(35 * tileSize, 2 * tileSize), 0f), 2),

                // SØSTER NOTER LEVEL 1
                new Note(false, NoteType.Tsima, "Notes/Tsima 5", new Transform(new Vector2(117 * tileSize, 39 * tileSize), 0f), 0),
                new Note(false, NoteType.Tsima, "Notes/Tsima 4", new Transform(new Vector2(93 * tileSize, 16 * tileSize), 0f), 0),
                new Note(false, NoteType.Tsima, "Notes/Tsima 2", new Transform(new Vector2(52 * tileSize, 63 * tileSize), 0f), 0),
                

                // PARENT NOTE LEVEL 1
                new Note(false, NoteType.Parents, "Notes/Parents note", new Transform(new Vector2(93 * tileSize, 9 * tileSize), 0f), 0),
                

                // SØSTER NOTER LEVEL 2
                new Note(false, NoteType.Tsima, "Notes/Tsima 3", new Transform(new Vector2(69 * tileSize, 3 * tileSize), 0f), 2),
                new Note(false, NoteType.Tsima, "Notes/Tsima 6", new Transform(new Vector2(110 * tileSize, 45 * tileSize), 0f), 2),
                new Note(false, NoteType.Tsima, "Notes/Tsima 1", new Transform(new Vector2(1 * tileSize, 51 * tileSize), 0f), 2),
                
            };
        }

    }
}
