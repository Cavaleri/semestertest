﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// The AnimatedGameObject class keeps track of all objects with animations
    /// </summary>
    public class AnimatedGameObject : GameObject
    {
        /// <summary>
        /// a dictionary with spritesheets
        /// </summary>
        Dictionary<string, Texture2D> animations;
        /// <summary>
        /// a dictionary that includes the number of frames in the spritesheet
        /// </summary>
        Dictionary<string, int> animationFrames;
        /// <summary>
        /// The animation that we go back to if nothing else is triggered
        /// </summary>
        string defaultAnimation;
        /// <summary>
        /// the frames per second that we want our animation to be in
        /// </summary>
        float animationFps;
        /// <summary>
        /// how much time has elapsed so far
        /// </summary>
        float timeElapsed;
        /// <summary>
        /// checks if we should loop the animation or not.
        /// </summary>
        bool loopAnimation;
        /// <summary>
        /// flips the object horizontally
        /// </summary>
        public bool flipAnimation;
        /// <summary>
        /// Rectangle around the frames of the spritesheet not the entier spritesheet
        /// </summary>
        public Rectangle[] Frames;
        /// <summary>
        /// the entier spritesheet
        /// </summary>
        public string CurrentAnimationKey { get; set; }
        /// <summary>
        /// a frame in the spritesheet
        /// </summary>
        public int CurrentAnimationIndex { get; set; }

        /// <summary>
        /// overides the hitbox so we can modify it to our liking
        /// </summary>
        public override Rectangle Hitbox
        {
            get { return new Rectangle((int)Transform.Position.X, (int)Transform.Position.Y, Sprite.Width / Frames.Length, Sprite.Height); }

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="spriteNames">name of the file of the spritesheet</param>
        /// <param name="animationFrames">holds spritesheet and devides it into parts equal to frames</param>
        /// <param name="animationFps">the frames per second of the animation</param>
        /// <param name="Transform">the position in the world</param>
        public AnimatedGameObject(List<string> spriteNames, Dictionary<string, int> animationFrames, float animationFps, Transform Transform) : base(Transform)
        {
            //allows us to get the sprites from contentmanger by name in our dictionary "animations"
            animations = new Dictionary<string, Texture2D>();
            foreach (string name in spriteNames)
            {
                Texture2D tempTexture = GameWorld.ContentManager.Load<Texture2D>(name);
                animations.Add(name, tempTexture);
            }

            this.animationFrames = animationFrames;
            this.defaultAnimation = spriteNames[0];

            this.animationFps = animationFps;
            this.timeElapsed = 0;
            this.loopAnimation = true;
            CurrentAnimationKey = spriteNames[0];
            CurrentAnimationIndex = 0;
            flipAnimation = false;

            base.Sprite = animations[spriteNames[0]];

            //draws rectangles so that we can draw individual frames from the spritesheet
            this.Frames = new Rectangle[animationFrames[CurrentAnimationKey]];
            for (int i = 0; i < Frames.Length; i++)
            {
                Frames[i] = new Rectangle(i * (animations[CurrentAnimationKey].Width / animationFrames[CurrentAnimationKey]), 0, (animations[CurrentAnimationKey].Width / animationFrames[CurrentAnimationKey]), animations[CurrentAnimationKey].Height);
            }
        }

        /// <summary>
        /// Get's the animation to run its own fps instead of following the one from the game. We do this with gametime to calculate our own fps.
        /// </summary>
        /// <param name="gameTime">time in game</param>
        public override void Update(GameTime gameTime)
        {
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            CurrentAnimationIndex = (int)(timeElapsed * animationFps);

            if(CurrentAnimationIndex > Frames.Length-1)
            {
                CurrentAnimationIndex = 0;
                timeElapsed = 0;

                if(loopAnimation == true)
                {
                    CurrentAnimationIndex = 0;
                    timeElapsed = 0;
                }
                else
                {
                    ChangeAnimation(defaultAnimation, true);
                }
            }
            
        }
        /// <summary>
        /// Draws the current spritesheet and frame also makes us able to flip the direction of the sprite back and forth horizontally
        /// </summary>
        /// <param name="spriteBatch">sprites!</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(Sprite, Transform.Position, Color.White);
            //spriteBatch.Draw(animations[CurrentAnimationKey], Transform.Position, Frames[CurrentAnimationIndex], Color.White);
            if(flipAnimation == true)
            {
                spriteBatch.Draw(animations[CurrentAnimationKey], new Vector2(Transform.Position.X, Transform.Position.Y - 1), Frames[CurrentAnimationIndex],Color.White,0,Vector2.Zero,new Vector2(1,1),SpriteEffects.FlipHorizontally,0);
            }
            else
            {
                spriteBatch.Draw(animations[CurrentAnimationKey], new Vector2(Transform.Position.X, Transform.Position.Y - 1), Frames[CurrentAnimationIndex], Color.White);
            }
        }
        /// <summary>
        /// Gives us the possibility so that we can change the animation from default to a new animation and loop that instead or just 
        /// power trough it once and go back to default
        /// </summary>
        /// <param name="animationKey"></param>
        /// <param name="shouldLoop">bool that either makes the animation that we change to loop or goes back to the default</param>
        public void ChangeAnimation(string animationKey, bool shouldLoop)
        {
           
            loopAnimation = shouldLoop;
            if (animationKey != CurrentAnimationKey)
            {
                if (base.Sprite.Height != animations[animationKey].Height)
                {
                    int dif = animations[animationKey].Height - base.Sprite.Height;
                    Transform.Position.Y -= dif;
                }
                if (base.Sprite.Width != animations[animationKey].Width && flipAnimation == true)
                {
                    int diff = (animations[animationKey].Width / animationFrames[animationKey]) - (base.Sprite.Width/animationFrames[CurrentAnimationKey]) ;
                    Transform.Position.X -= diff;
                }
                base.Sprite = animations[animationKey];
                CurrentAnimationIndex = 0;
                timeElapsed = 0;
                CurrentAnimationKey = animationKey;
                this.Frames = new Rectangle[animationFrames[animationKey]];
                for (int i = 0; i < Frames.Length; i++)
                {
                    Frames[i] = new Rectangle(i * (animations[animationKey].Width / animationFrames[animationKey]), 0, (animations[animationKey].Width / animationFrames[animationKey]), animations[animationKey].Height);
                }
            }
        }


    }
}
