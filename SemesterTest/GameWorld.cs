﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace SemesterTest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameWorld : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        /// <summary>
        /// The texture for the Player
        /// </summary>
        public Texture2D playerTexture;

        /// <summary>
        /// The texture for the fireball
        /// </summary>
        public Texture2D fireballTexture;

        /// <summary>
        /// Transform for the player starting position
        /// </summary>
        public static Transform playerTransform = new Transform(new Vector2(50, 1040), new Vector2(), 1f); 
        string playerString = "Player_small";
        string fireballString = "Fireball_1";
        Player thePlayer;


        private Texture2D statusFrameTexture;
        private Texture2D statusHealthTexture;
        private Texture2D statusManaTexture;
        

        private Texture2D winZarfurNotes;
        private Texture2D winZarfurTsimaNotes;
        private Texture2D winZarfurParentsNotes;
        private Texture2D winZarfurTsimaParentsNotes;
        private Texture2D splashScreen;
        private Texture2D introStory;
        //private SoundEffectInstance sadTrombone;
        private bool tromboneAlreadyPlayed;

        private Texture2D tutorialBook;

        /// <summary>
        /// Texture for the collision boxes
        /// </summary>
        public Texture2D collisionTexture;

        private List<GameObject> gameObjects;
        private static List<GameObject> addList = new List<GameObject>();
        private static List<GameObject> removeList = new List<GameObject>();

        private Tilemap tilemap;

        private static bool gameOver;
        private static bool didWin;
        private static int sisterNotesCollected;
        private static int parentNotesCollected;
        private SpriteFont font;
        private SpriteFont smallFont;
        private SpriteFont tutorialFont;

        private static bool shouldChangeLevel;
        private static int numberOfZarfurNotesPickedUp;

        public static SoundEffectInstance stoneSlideSound;

        private static ContentManager _content;
        /// <summary>
        /// List of projectile texture names
        /// </summary>
        public static List<string> projectileName;
        /// <summary>
        /// List of projectile texture names and frames
        /// </summary>
        public static Dictionary<string, int> fireballAnimations;
        public static List<string> fiDi;
        public static Dictionary<string, int> fiDiAn;
        private Vector2 Direction;
        private static Song backTrack;
        private static Song christmasTrack;
        private bool tutorial = true;
        private bool splash = true;
        private bool intro = true;
        private bool enterReleased = true;

        /// <summary>
        /// ContentManager to load assets dynamically
        /// </summary>
        public static ContentManager ContentManager
        {
            get => _content;
        }

        /// <summary>
        ///  Creates a new instance of the  GameWorld
        /// </summary>
        public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;
            graphics.IsFullScreen = true;

            Content.RootDirectory = "Content";

            _content = Content;
            gameObjects = new List<GameObject>();

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            tilemap = new Tilemap();

            ResetGame();

            ///music
            backTrack = ContentManager.Load<Song>("CrimsonSound1.1");
            christmasTrack = ContentManager.Load<Song>("we-wish-you-a-merry-christmas");

            MediaPlayer.Volume = 0.32f;
            MediaPlayer.Play(backTrack);
            MediaPlayer.IsRepeating = true;

            //Soundeffects
            stoneSlideSound = ContentManager.Load<SoundEffect>("Stone_slide_sound").CreateInstance();

            //Collision
            collisionTexture = Content.Load<Texture2D>("OnePixel");

            // TODO: use this.Content to load your game content here
            statusFrameTexture = Content.Load<Texture2D>("bar_frame");
            statusHealthTexture = Content.Load<Texture2D>("bar_hp");
            statusManaTexture = Content.Load<Texture2D>("bar_mp");

            winZarfurNotes = Content.Load<Texture2D>("notes_zarfur");
            winZarfurTsimaNotes = Content.Load<Texture2D>("notes_zarfur_tsima");
            winZarfurParentsNotes = Content.Load<Texture2D>("notes_zarfur_parents");
            winZarfurTsimaParentsNotes = Content.Load<Texture2D>("notes_zarfur_tsima_parents");
            tutorialBook = Content.Load<Texture2D>("spellbookForFlare");
            splashScreen = Content.Load<Texture2D>("splash");
            introStory = Content.Load<Texture2D>("Intro_story");
            //sadTrombone = Content.Load<SoundEffect>("SadTrombone").CreateInstance();

            font = Content.Load<SpriteFont>("ExampleFontx5");
            tutorialFont = Content.Load<SpriteFont>("ExampleFontx4");
            smallFont = Content.Load<SpriteFont>("ExampleFont");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            if (splash == true)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Enter) || GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.Start))
                {
                    if (enterReleased)
                    {
                        splash = false;
                        enterReleased = false;
                    }
                }
                else
                {
                    enterReleased = true;
                }
            }
            else if (tutorial == true)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Enter) || GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.Start))
                {
                    if (enterReleased)
                    {
                        tutorial = false;
                        enterReleased = false;
                    }
                }
                else
                {
                    enterReleased = true;
                }
            }
            else if (intro == true)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Enter) || GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.Start))
                {
                    if (enterReleased)
                    {
                        intro = false;
                        enterReleased = false;
                    }
                }
                else
                {
                    enterReleased = true;
                }
            }
            else if (!gameOver)
            {
                // Run update loop for all game objects
                foreach (GameObject obj in gameObjects)
                {
                    obj.Update(gameTime);

                    if (obj is Player)
                    {
                        // Update the line of sight
                        tilemap.CalculateLineOfSight(obj.Hitbox.Center, ((Player)obj).LineOfSight);
                    }
                    //else if (obj is Projectile)
                    //{
                    //    tilemap.AppendLineOfSight(obj.Hitbox.Center, ((Projectile)obj).LineOfSight);
                    //}
                }


                // Check for sound playability
                foreach (GameObject obj in gameObjects)
                {
                    if (!(obj is Player))
                    {
                        obj.CheckForSoundPlayability(thePlayer.Hitbox);
                    }
                }

                // Check collision with tilemap
                foreach (Tile tile in tilemap.VisibleTiles)
                {
                    foreach (GameObject obj in gameObjects)
                    {
                        if (obj is Player && obj.CheckCollision(tile) || obj is Projectile && obj.CheckCollision(tile))
                        {
                            obj.OnCollision(tile);
                        }
                    }
                }

                // Check for collision
                foreach (GameObject obj in gameObjects)
                {
                    foreach (GameObject other in gameObjects)
                    {
                        // Check for collision and avoid colliding with self
                        if (obj != other && obj.CheckCollision(other))
                        {
                            // Handle collision if applicable
                            obj.OnCollision(other);
                        }

                        if (obj is EasterEgg && other is Player && ((EasterEgg)obj).CheckCollisionWithChristmas(other))
                        {
                            ((EasterEgg)obj).OnCollisionWithChristmas(other);
                        }
                    }
                }


                // Remove all game objects in removeList
                foreach (GameObject obj in removeList)
                {
                    gameObjects.Remove(obj);
                }
                removeList.Clear();


                // Add all game obejcts in addList
                foreach (GameObject obj in addList)
                {
                    gameObjects.Add(obj);
                }
                addList.Clear();


                // Change level if a note was found which changes levels
                if (shouldChangeLevel)
                {
                    ChangeLevel();
                }
            }
            else
            {
                if (!didWin)
                {
                    if (! tromboneAlreadyPlayed)
                    {
                        //sadTrombone.Play();
                        tromboneAlreadyPlayed = true;
                    }
                }

                if (Keyboard.GetState().IsKeyDown(Keys.R)||GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.Y))
                {
                    ResetGame();
                    tutorial = true;
                    splash = true;
                    intro = true;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            // Draw intro screen
            if (splash)
            {
                spriteBatch.Draw(splashScreen, Vector2.Zero, Color.White);

                spriteBatch.DrawString(smallFont, "Press Start to continue", new Vector2(320, 900), Color.Black);
            }
            // Draw tutorial text
            else if (tutorial)
            {
                spriteBatch.Draw(tutorialBook, new Vector2((graphics.PreferredBackBufferWidth - tutorialBook.Width) / 2, (graphics.PreferredBackBufferHeight - tutorialBook.Height) / 2), Color.White);

                spriteBatch.DrawString(tutorialFont, "Move", new Vector2(320, 230), Color.Black);
                spriteBatch.DrawString(tutorialFont, "Jump", new Vector2(320, 360), Color.Black);
                spriteBatch.DrawString(tutorialFont, "Fire", new Vector2(320, 490), Color.Black);

                spriteBatch.DrawString(tutorialFont, ":", new Vector2(620, 230), Color.Black);
                spriteBatch.DrawString(tutorialFont, ":", new Vector2(620, 360), Color.Black);
                spriteBatch.DrawString(tutorialFont, ":", new Vector2(620, 490), Color.Black);

                spriteBatch.DrawString(tutorialFont, "Left Joystick", new Vector2(1020, 230), Color.Black);
                spriteBatch.DrawString(tutorialFont, "X", new Vector2(1020, 360), Color.Black);
                spriteBatch.DrawString(tutorialFont, "Square", new Vector2(1020, 490), Color.Black);


                spriteBatch.DrawString(smallFont, "Press Start to continue", new Vector2(320, 800), Color.Black);

                // spriteBatch.DrawString(tutorialFont, "Move - WASD / arrow keys\nJump - spacebar / up\nFireball - c\nTo continue - Enter", new Vector2(20, 60), Color.White);
            }
            else if (intro)
            {
                spriteBatch.Draw(introStory, new Vector2((graphics.PreferredBackBufferWidth - introStory.Width) / 2, (graphics.PreferredBackBufferHeight - introStory.Height) / 2), Color.White);

                spriteBatch.DrawString(smallFont, "Press Start to continue", new Vector2(320, 900), Color.Black);
            }
            // Draw tutorial text
            else if (!gameOver)
            {
                // Draw background
                tilemap.DrawLevel(spriteBatch, true);


                // Iterate and draw each object
                foreach (GameObject obj in gameObjects)
                {
                    obj.Draw(spriteBatch);
                }

                //Collision texture draw
                foreach (GameObject go in gameObjects)
                {
                    go.Draw(spriteBatch);
#if DEBUG
                    DrawCollisionBox(go);
#endif
                }


                // Draw fog of war
                tilemap.DrawFogOfWar(spriteBatch);


                // Draw the players status bar
                DrawStatusBar(spriteBatch);

                // Force notes to be drawn outside line of sight if shown
                foreach (GameObject note in gameObjects)
                {
                    if (note is Note)
                    {
                        Note thisNote = (Note) note;
                        if (thisNote.DrawNote)
                        {
                            thisNote.Draw(spriteBatch);
                        }
                    }
                }

            }
            else
            {
                if (didWin)
                {
                    // Only all zarfur notes was collected
                    if (sisterNotesCollected < 6 && parentNotesCollected < 1)
                    {
                        spriteBatch.Draw(winZarfurNotes, Vector2.Zero, Color.White);
                    }
                    // Only zarfur + parents notes was collected
                    else if (sisterNotesCollected < 6 && parentNotesCollected == 1)
                    {
                        spriteBatch.Draw(winZarfurParentsNotes, Vector2.Zero, Color.White);
                    }
                    // Only zarfur notes and all sister notes was collected
                    else if (sisterNotesCollected == 6 && parentNotesCollected < 1)
                    {
                        spriteBatch.Draw(winZarfurTsimaNotes, Vector2.Zero, Color.White);
                    }
                    // All notes were collected
                    else if (sisterNotesCollected == 6 && parentNotesCollected == 6)
                    {
                        spriteBatch.Draw(winZarfurTsimaParentsNotes, Vector2.Zero, Color.White);
                    }

                    spriteBatch.DrawString(smallFont, "You have collected " + (sisterNotesCollected + parentNotesCollected) + " / 7 secret notes. Triangle to play again", new Vector2(300, 1000), Color.Crimson);

                }
                else
                {
                    spriteBatch.DrawString(font, "Got lost in the dark?\nTriangle to try again", new Vector2(300, 60), Color.Crimson);
                }
            }

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        /// <summary>
        /// Handles game over logic
        /// </summary>
        /// <param name="didWin">Whether the game was won or not</param>
        public static void GameOver(bool didWin)
        {
            gameOver = true;
            GameWorld.didWin = didWin;
        }

        /// <summary>
        /// Used to spawn items based on level
        /// </summary>
        private void ChangeLevel()
        {
            shouldChangeLevel = false;

            tilemap.ChangeLevel();

            // Only change enemies on correct levels
            if (tilemap.CurrentLevel != 1 && tilemap.CurrentLevel != 3)
            {
                // Remove all excess enemies and notes
                foreach (GameObject obj in gameObjects)
                {
                    if (obj is Enemy || obj is Note || obj is EasterEgg)
                    {
                        RemoveGameObject(obj);
                    }
                }

                // Add all enemies
                foreach (Enemy obj in StaticContent.StaticListOfEnemies)
                {
                    if (obj.Level == tilemap.CurrentLevel)
                    {
                        AddGameObject(obj);
                    }
                }

                // Add all notes
                foreach (Note obj in StaticContent.StaticListOfNotes)
                {
                    if (obj.Level == tilemap.CurrentLevel)
                    {
                        AddGameObject(obj);
                    }
                }

                // Add NPC if last level
                if (tilemap.CurrentLevel == 2)
                {
                    foreach (Siblings sibling in StaticContent.StaticListOfSiblings)
                    {
                        AddGameObject(sibling);
                    }
                }

                // Add easter egg
                if (tilemap.CurrentLevel == 0)
                {
                    AddGameObject(StaticContent.ChristmasTree);
                }
            }
        }

        /// <summary>
        /// Used to reset all game status when lost / won
        /// </summary>
        private void ResetGame()
        {
            gameObjects.Clear();
            addList.Clear();
            removeList.Clear();

            numberOfZarfurNotesPickedUp = 0;

            // HMMM I hate this coding business.... :(
            tilemap.CurrentLevel = -1;
            StaticContent.RepopulateStaticLists();

            ChangeLevel();

            //Player
            List<string> temppla = new List<string>();
            temppla.Add("Player_idle");
            temppla.Add("Player_walk");
            temppla.Add("Player_attack");
            Dictionary<string, int> templaani = new Dictionary<string, int>();
            templaani.Add("Player_idle", 4);
            templaani.Add("Player_walk", 8);
            templaani.Add("Player_attack", 2);
            thePlayer = new Player(temppla, templaani, 10, playerTransform);
            gameObjects.Add(thePlayer);

            //Projectile
            projectileName = new List<string>();
            projectileName.Add("Fireball_1");
            fiDi = new List<string>();
            fiDi.Add("Fireball_Dissolve");
            fireballAnimations = new Dictionary<string, int>();
            fireballAnimations.Add("Fireball_1", 1);
            fiDiAn = new Dictionary<string, int>();
            fiDiAn.Add("Fireball_Dissolve", 5);


            didWin = false;
            gameOver = false;
            shouldChangeLevel = false;
            tromboneAlreadyPlayed = false;
            sisterNotesCollected = 0;
            parentNotesCollected = 0;
        }

        /// <summary>
        /// Adds a new GameObject in queue to be added to list of GameObjects
        /// </summary>
        /// <param name="obj">The object to be added</param>
        public static void AddGameObject(GameObject obj)
        {
            addList.Add(obj);
        }

        /// <summary>
        /// Adds a new GameObject in queue to be removed from list of GameObjects
        /// </summary>
        /// <param name="obj">The object to be removed</param>
        public static void RemoveGameObject(GameObject obj)
        {
            removeList.Add(obj);

        }

        public static void ChangeBacktrack(bool christmas)
        {
            if (christmas)
            {
                MediaPlayer.Play(christmasTrack);
            }
            else
            {
                MediaPlayer.Play(backTrack);
            }
        }

        /// <summary>
        /// Handles logic when picking up a Note
        /// </summary>
        /// <param name="note"></param>
        public static void PickedUpNote(Note note)
        {
            if (note.ChangesLevel)
            {
                shouldChangeLevel = true;
            }

            if (note.Type == NoteType.Zarfur)
            {
                numberOfZarfurNotesPickedUp += 1;

                if (numberOfZarfurNotesPickedUp == 4 || numberOfZarfurNotesPickedUp == 10)
                {
                    shouldChangeLevel = true;
                    stoneSlideSound.Play();
                }
            }
            else if (note.Type == NoteType.Tsima)
            {
                sisterNotesCollected += 1;
            }
            else if (note.Type == NoteType.Parents)
            {
                parentNotesCollected += 1;
            }
        }

        /// <summary>
        /// Used to draw health and mana bar above Player
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch</param>
        private void DrawStatusBar(SpriteBatch spriteBatch)
        {
            Vector2 hpBarPos = new Vector2(thePlayer.Hitbox.Center.X - (statusFrameTexture.Width * 0.5f), thePlayer.Hitbox.Center.Y - 35);
            Vector2 mpBarPos = new Vector2(thePlayer.Hitbox.Center.X - (statusFrameTexture.Width * 0.5f), thePlayer.Hitbox.Center.Y - 28);

            spriteBatch.Draw(statusFrameTexture, hpBarPos, Color.White);
            for (int i = 0; i < thePlayer.HealthPoints; i++)
            {
                spriteBatch.Draw(statusHealthTexture, new Vector2((hpBarPos.X + (statusHealthTexture.Width * i)), hpBarPos.Y), Color.White);
            }

            spriteBatch.Draw(statusFrameTexture, mpBarPos, Color.White);
            for (int i = 0; i < thePlayer.ManaPoints; i++)
            {
                spriteBatch.Draw(statusManaTexture, new Vector2((mpBarPos.X + (statusManaTexture.Width * i)), mpBarPos.Y), Color.White);
            }

        }

        /// <summary>
        /// DrawCollisionBox draws a line around the given texture, used for debugging any collision issues
        /// </summary>
        /// <param name="go"></param>

        public void DrawCollisionBox(GameObject go)
        {
            //Creating a box around the object
            Rectangle collisionBox = go.Hitbox;

            Rectangle topLine = new Rectangle(collisionBox.Center.X - collisionBox.Width / 2, collisionBox.Center.Y - collisionBox.Height / 2, collisionBox.Width, 1);
            Rectangle bottomLine = new Rectangle(collisionBox.Center.X - collisionBox.Width / 2, collisionBox.Center.Y + collisionBox.Height / 2, collisionBox.Width, 1);
            Rectangle rightLine = new Rectangle(collisionBox.Center.X + collisionBox.Width / 2, collisionBox.Center.Y - collisionBox.Height / 2, 1, collisionBox.Height);
            Rectangle leftLine = new Rectangle(collisionBox.Center.X - collisionBox.Width / 2, collisionBox.Center.Y - collisionBox.Height / 2, 1, collisionBox.Height);

            spriteBatch.Draw(collisionTexture, topLine, null, Color.Red, 0, Vector2.Zero, SpriteEffects.None, 1);
            spriteBatch.Draw(collisionTexture, bottomLine, null, Color.Red, 0, Vector2.Zero, SpriteEffects.None, 1);
            spriteBatch.Draw(collisionTexture, rightLine, null, Color.Red, 0, Vector2.Zero, SpriteEffects.None, 1);
            spriteBatch.Draw(collisionTexture, leftLine, null, Color.Red, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

    }

}
