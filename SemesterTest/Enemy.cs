﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// The Enemy class keeps track of all enemy objects
    /// </summary>
    public class Enemy : AnimatedGameObject
    {
        /// <summary>
        /// Checks if the enemy is attacking
        /// </summary>
        bool isAttacking;
        /// <summary>
        /// a value to calculate damage
        /// </summary>
        float attackDamage;

        SoundEffectInstance skeletonAttackSound = GameWorld.ContentManager.Load<SoundEffect>("Skeleton_attack_sound").CreateInstance();
        
        /// <summary>
        /// A pos that we use to patrol between
        /// </summary>
        Vector2 Start;
        /// <summary>
        /// the other pos we use to patrol
        /// </summary>
        Vector2 End;
        /// <summary>
        /// the speed at witch our enemy moves
        /// </summary>
        float speed;
        /// <summary>
        /// Checks what map we are on
        /// </summary>
        public int Level;
        /// <summary>
        /// sets the amount of hp our enemy has
        /// </summary>
        public int Health { get; set; }
        /// <summary>
        /// We flip the animation depending on what direction the sprite is moving.
        /// </summary>
        public float Speed {
            get { return speed; }
            set
            {
                speed = value;
                if (value < 0)
                {
                    flipAnimation = true;
                }
                else
                {
                    flipAnimation = false;
                }
            }
        }
        /// <summary>
        /// Constructor for enemy
        /// </summary>
        /// <param name="spriteNames">The name of the sprite</param>
        /// <param name="animationFrames">The amount of frames in a sprite sheet</param>
        /// <param name="startingFPS">the fps for the animation</param>
        /// <param name="Start">A position where our enemy will turn around when he reaches</param>
        /// <param name="End">a Position where our enemy will turn around when he reaches</param>
        /// <param name="Transform">the actual position where he starts</param>
        public Enemy(List<string> spriteNames, Dictionary<string, int> animationFrames, float startingFPS,Vector2 Start,Vector2 End, Transform Transform, int level) 
            : base(spriteNames, animationFrames, startingFPS, Transform)
        {
            this.Start = Start;
            this.End = End;
            this.Level = level;

            //this.isAttacking = true;
            //this.attackDamage = 1;
            Health = 4;
            speed = 1;
            attackDamage = 1;

            base.soundEffect = GameWorld.ContentManager.Load<SoundEffect>("Skeleton_walk_sound").CreateInstance();
            base.soundRadius = 150f; // 260 i virkeligheden > 250 i den rigtige virkelighed
        }

        /// <summary>
        /// Disable tile collision for enemies
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool CheckCollision(Tile other)
        {
            return false;
        }

        /// <summary>
        /// Makes the enemy switch to the attack animation when he touches a player and calls the attack function when it reaches the 8th frame
        /// </summary>
        /// <param name="other">other gameobjects</param>
        public override void OnCollision(GameObject other)
        {
            if (other is Player)
            {
                if(CurrentAnimationKey=="Skeleton Dead")
                {

                }
                else
                {
                    ChangeAnimation("Skeleton Attack", false);
                    skeletonAttackSound.Play();
                    if (CurrentAnimationIndex == 7)
                    {
                        Attack((Player)other);
                        
                    }
                }
                
            }

            if (other is Projectile)
            {
                

            }
        }
        /// <summary>
        /// makes the enemy able to take damage and if the health reaches 0 or below it dies.
        /// </summary>
        /// <param name="dmg">damage taken from sources of damage</param>
        public void TakeDamage(float dmg)
        {
            
            if (Health >= 1)
            {
                Health -= (int)dmg;
            }
            if (Health <= 0)
            {
                speed = 0;
                ChangeAnimation("Skeleton Dead", false);
            }
        }
        /// <summary>
        /// gives damage to anyone who stands in the arc of damage during the 8th frame of the attack animation
        /// </summary>
        /// <param name="player">The players charecter</param>
        public void Attack(Player player)
        {
            player.TakeDamage(attackDamage);
        }

        //Sound

        /// <summary>
        /// Here we set the default behavoir of the enemy so that it can walk/patrol between 2 points
        /// </summary>
        /// <param name="gameTime">time in game</param>
        public override void Update(GameTime gameTime)
        {
            if(CurrentAnimationKey=="Skeleton Dead")
            {

                if (CurrentAnimationIndex == 14)
                {
                    GameWorld.RemoveGameObject(this);

                    soundEffect.Stop();
                }
            }
            else if (CurrentAnimationKey=="Skeleton Walk")
            {
                Transform.Position.X += Speed;
            
                if (Transform.Position == Start)
                {
                    Speed *= -1;
                }
                else if(Transform.Position == End)
                {
                    Speed *= -1;
                }
            }

            /*if (Microsoft.Xna.Framework.Input.Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Z))
            {
                base.ChangeAnimation("Skeleton Walk", true);
            }
            else if (Microsoft.Xna.Framework.Input.Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.X))
            {
                
                base.ChangeAnimation("Skeleton Attack", false);
            }
            else if (Microsoft.Xna.Framework.Input.Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.C))
            {
                Health = -42;
                base.ChangeAnimation("Skeleton Dead", false);
            }*/

            base.Update(gameTime);



        }



    }
}
