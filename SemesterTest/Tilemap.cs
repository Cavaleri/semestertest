﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// Tilemap class to control the level design
    /// </summary>
    public class Tilemap
    {
        private string baseLevelPath = @"levels\";
        private string platformsName = "Platforms.csv";
        private string fakePlatformsName = "FakePlatforms.csv";
        private string backgroundsName = "Background.csv";
       

        private const int tempImageWidth = 17;
        private const int tileSize = 16;
        private const int mapSizeColumns = 120;
        private const int mapSizeRows = 68;

        private Texture2D tilesheet;
        private Tile[,] backgroundTiles;
        private Tile[,] platformTiles;
        private Tile[,] platformFakeTiles;

        private Tile cachedStartTile;
        private HashSet<Tile> cachedDrawTiles;
        private bool sightAppended = false;

        private Tile[,] tiles;
        private Tile[,] tilesReverse;

        /// <summary>
        /// Two dimensional array with all tiles in the current level
        /// </summary>
        public Tile[,] Tiles
        {
            get => tiles;
        }
        /// <summary>
        /// Two dimensional array with all tiles in the current level in reverse order
        /// </summary>
        public Tile[,] TilesReverse
        {
            get => tilesReverse;
        }
        /// <summary>
        /// Hashset with all the visible tiles
        /// </summary>
        public HashSet<Tile> VisibleTiles
        {
            get => cachedDrawTiles;
        }
            

        /// <summary>
        /// Current level index
        /// </summary>
        public int CurrentLevel;

        /// <summary>
        /// Creates new instance of the Tilemap class
        /// </summary>
        public Tilemap()
        {
            this.tilesheet = GameWorld.ContentManager.Load<Texture2D>("cavesofgallet_tiles");

            this.CurrentLevel = 0;

            this.GenerateLevels(this.CurrentLevel);
            this.cachedDrawTiles = new HashSet<Tile>();
        }

        /// <summary>
        /// Draw tiles in map based on a line of sight from a point of view
        /// </summary>
        /// <param name="spriteBatch">Spritebatch from the GameWorld</param>
        /// <param name="drawLOS">Determines if line of sight or full map should be drawn</param>
        public void DrawLevel(SpriteBatch spriteBatch, bool drawLOS)
        {
            // Draw full map
            if (! drawLOS)
            {
                foreach (Tile tile in tiles)
                {
                    if (tile != null)
                    {
                        spriteBatch.Draw(tilesheet, tile.Transform.Position, tile.SpriteRect, Color.White);
                    }
                }
            }
            // Draw only line of sight
            else
            {
                foreach (Tile tile in cachedDrawTiles)
                {
                    if (tile != null)
                    {
                        spriteBatch.Draw(tilesheet, tile.Transform.Position, tile.SpriteRect, Color.White);
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the line of sight from a point of view
        /// </summary>
        /// <param name="pointOfView">Point to calculate tiles from</param>
        /// <param name="lineOfSight">Length of line of sight to traverse in each direction</param>
        public void CalculateLineOfSight (Point pointOfView, int lineOfSight)
        {
            HashSet<Tile> drawTiles;
            Tile startTile = GetTileAtXY(pointOfView.X, pointOfView.Y);

            // Use cache unless it is necessary to calculate new LOS
            if (startTile == cachedStartTile && !sightAppended)
            {
                drawTiles = cachedDrawTiles;
            }
            else
            {
                // Calculate new tiles to draw based on line of sight
                drawTiles = FloodFillTiles(startTile, lineOfSight);

                // Set new cache
                cachedStartTile = startTile;
                cachedDrawTiles = drawTiles;
                sightAppended = false;
            }
        }

        /// <summary>
        /// Append extra tiles to the line of sight
        /// </summary>
        /// <param name="pointOfView">Point to calculate tiles from</param>
        /// <param name="lineOfSight">Length of line of sight to traverse in each direction</param>
        public void AppendLineOfSight (Point pointOfView, int lineOfSight)
        {
            Tile startTile = GetTileAtXY(pointOfView.X, pointOfView.Y);
            HashSet<Tile> drawTiles = FloodFillTiles(startTile, lineOfSight);

            foreach (Tile tile in drawTiles)
            {
                cachedDrawTiles.Add(tile);
            }

            sightAppended = true;
        }

        /// <summary>
        /// Draw black overlay on all tiles out
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch from GameWorld</param>
        public void DrawFogOfWar(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    Tile tile = tiles[i, j];

                    if (! cachedDrawTiles.Contains(tile))
                    {
                        spriteBatch.Draw(tilesheet, tile.Transform.Position, tile.SpriteRect, Color.Black);
                    }
                }
            }
        }

        private HashSet<Tile> FloodFillTiles(Tile startTile, int lineOfSight)
        {
            HashSet<Tile> drawTiles = new HashSet<Tile>();
            HashSet<Tile> needToVisit = new HashSet<Tile>();

            // Find preliminary neighbours to first tile
            drawTiles.Add(startTile);
            Tile[] demoTiles = GetNeighbourTiles(startTile);
            foreach (Tile tile in demoTiles)
            {
                if (tile != null)
                {
                    drawTiles.Add(tile);
                }
            }

            // Iterate over each increase in line of sight
            for (int i = 0; i < lineOfSight; i++)
            {
                // Look at all tiles already setup to be drawn
                foreach (Tile drawTile in drawTiles)
                {
                    // Don't add tiles on the other side of a collideable
                    if (drawTile != null && !drawTile.Collideable && !drawTile.FakeCollidable)
                    {
                        // Find their neighbours and iterate over all of these
                        Tile[] neighbours = GetNeighbourTiles(drawTile);

                        foreach (Tile neighbour in neighbours)
                        {
                            needToVisit.Add(neighbour);
                        }
                    }
                }

                // Add each neighbour to the list of tiles to draw
                foreach (Tile newTile in needToVisit)
                {
                    drawTiles.Add(newTile);
                }
                needToVisit.Clear();
            }

            return drawTiles;
        }

        /// <summary>
        /// Returns a tile based on x/y coordinate from game world
        /// </summary>
        /// <param name="x">X-coordinate to search</param>
        /// <param name="y">Y-coordinate to search</param>
        /// <returns>Tile or null if not found</returns>
        private Tile GetTileAtXY(float x, float y)
        {
            int i = (int) Math.Floor(y / tileSize);
            int j = (int) Math.Floor(x / tileSize);

            if (i >= 0 && i < mapSizeRows && j >=0 && j < mapSizeColumns)
            {
                return tiles[i, j];
            }
            return null;
        }

        /// <summary>
        /// Get all neighbouring tiles in each direction
        /// </summary>
        /// <param name="tile">The tile to search neighbours from</param>
        /// <returns></returns>
        private Tile[] GetNeighbourTiles(Tile tile)
        {
            Tile[] neighbours = new Tile[4];

            // Left
            neighbours[0] = GetTileAtXY(tile.Hitbox.Center.X - tileSize, tile.Hitbox.Center.Y);
            // Up
            neighbours[1] = GetTileAtXY(tile.Hitbox.Center.X, tile.Hitbox.Center.Y - tileSize);
            // Right
            neighbours[2] = GetTileAtXY(tile.Hitbox.Center.X + tileSize, tile.Hitbox.Center.Y);
            // Down
            neighbours[3] = GetTileAtXY(tile.Hitbox.Center.X, tile.Hitbox.Center.Y + tileSize);

            return neighbours;
        }

        /// <summary>
        /// Generate levels from each layer
        /// </summary>
        /// <param name="level">The current level index</param>
        private void GenerateLevels(int level)
        {
            this.backgroundTiles = new Tile[mapSizeRows, mapSizeColumns];
            this.platformTiles = new Tile[mapSizeRows, mapSizeColumns];
            this.platformFakeTiles = new Tile[mapSizeRows, mapSizeColumns];

            this.tiles = new Tile[mapSizeRows, mapSizeColumns];

            // Generate private helper lists
            backgroundTiles = GenerateTiles(baseLevelPath + level.ToString() + @"\" + backgroundsName, false, false, false);
            platformTiles = GenerateTiles(baseLevelPath + level.ToString() + @"\" + platformsName, true, false, false);
            platformFakeTiles = GenerateTiles(baseLevelPath + level.ToString() + @"\" + fakePlatformsName, false, true, false);


            // Setup current visible map tiles
            SetupPublicMapTiles();
        }

        /// <summary>
        /// Change to next level
        /// </summary>
        public void ChangeLevel ()
        {
            this.CurrentLevel += 1;

            this.GenerateLevels(this.CurrentLevel);
        }

        /// <summary>
        /// Sets all tiles from each layer to a single two-dimensional array
        /// 
        /// Note: If two or more layers have a tile in the same spot, only one will be added based on the following priority
        /// 1. Platform tiles
        /// 2. Stair tiles
        /// 3. Fake platform tiles
        /// 4. Background tiles
        /// </summary>
        private void SetupPublicMapTiles()
        {
            // For each row
            for (int i = 0; i < mapSizeRows; i++)
            {
                // For each column
                for (int j = 0; j < mapSizeColumns; j++)
                {
                    // Add the tile to the list of all tiles based on priority
                    this.tiles[i, j] = (platformTiles[i, j] != null) ? platformTiles[i, j] :
                                         (platformFakeTiles[i, j] != null) ? platformFakeTiles[i, j] :
                                         (backgroundTiles[i, j] != null) ? backgroundTiles[i, j] :
                                         null;
                }
            }

            // Setup tiles in reverse order
            tilesReverse = new Tile[tiles.GetLength(0), tiles.GetLength(1)];
            for (int h = 0; h < tilesReverse.GetLength(0); h++)//height
            {
                for (int j = (tilesReverse.GetLength(0) - 1) - h; j >= 0; j--)//height
                {
                    for (int l = 0; l < tiles.GetLength(1); l++)
                    {
                        tilesReverse[j, l] = tiles[h, l];
                    }
                }
            }
        }

        /// <summary>
        /// Returns tiles from layer based on a CSV file input
        /// </summary>
        /// <param name="path">path to CSV file</param>
        /// <param name="collidable">Whether tile is collideable</param>
        /// <param name="fakeCollidable">Whether tile is platform but not collideable</param>
        /// <param name="climbable">Whether tile is climbable</param>
        /// <returns></returns>
        private Tile[,] GenerateTiles(string path, bool collidable, bool fakeCollidable, bool climbable)
        {
            string[] lines = File.ReadAllLines(path);

            Tile[,] tempTileset = new Tile[mapSizeRows, mapSizeColumns];

            // Loop through each line in the CSV file and split each line by comma
            for (int i = 0; i < lines.Length; i++)
            {
                string[] line = lines[i].Split(',');

                // Loop through each cell
                for (int j = 0; j < line.Length; j++)
                {
                    if (line[j] != "-1")
                    {
                        int index = Convert.ToInt32(line[j]);

                        int row = (int) Math.Floor((float) (index / tempImageWidth));
                        int col = (int)(index % tempImageWidth);

                        int x = j * tileSize;
                        int y = i * tileSize;

                        Rectangle rectangle = new Rectangle(col * tileSize, row * tileSize, tileSize, tileSize);
                        tempTileset[i,j] = new Tile(collidable, fakeCollidable, climbable, rectangle, new Transform(new Vector2(x, y), 0f));
                    }
                }
            }

            return tempTileset;
        }

    }

}