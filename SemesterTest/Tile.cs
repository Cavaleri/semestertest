﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// A tile used in the Tilemap
    /// </summary>
    public class Tile
    {
        private bool collideable;
        private bool fakeCollidable;
        private bool climbable;
        private Rectangle spriteRect;
        private Transform transform;

        /// <summary>
        /// Whether this tile is a platform
        /// </summary>
        public bool Collideable
        {
            get => collideable;
        }
        /// <summary>
        /// Whether this tile is a fake platform
        /// </summary>
        public bool FakeCollidable
        {
            get => fakeCollidable;
        }
        /// <summary>
        /// Whether this tile is climbable
        /// </summary>
        public bool Climbable
        {
            get => climbable;
        }
        /// <summary>
        /// The tiles rectangle to draw from the full tileset in Tilemap class
        /// </summary>
        public Rectangle SpriteRect
        {
            get => spriteRect;
        }
        /// <summary>
        /// Tiles position in the game world
        /// </summary>
        public Transform Transform
        {
            get => transform;
        }
        /// <summary>
        /// AABB collision box
        /// </summary>
        public virtual Rectangle Hitbox
        {
            get { return new Rectangle((int)Transform.Position.X, (int)Transform.Position.Y, SpriteRect.Width, SpriteRect.Height); }
        }

        /// <summary>
        /// Creates new instance of the Tile
        /// </summary>
        /// <param name="collideable">Whether tile is a collidable platform</param>
        /// <param name="fakeCollidable">Whether tile is a platform that does not collide</param>
        /// <param name="climbable">Whether tile is climbable</param>
        /// <param name="spriteRect">The part of the tileset to draw</param>
        /// <param name="transform">Where to place the Tile in gameworld</param>
        public Tile(bool collideable, bool fakeCollidable, bool climbable, Rectangle spriteRect, Transform transform)
        {
            this.collideable = collideable;
            this.fakeCollidable = fakeCollidable;
            this.climbable = climbable;
            this.spriteRect = spriteRect;
            this.transform = transform;
        }
    }
}
