﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// Enum for different types of notes
    /// </summary>
    public enum NoteType { Zarfur, Tsima, Parents }

    /// <summary>
    /// The notes to be found in the game
    /// </summary>
    public class Note : GameObject
    {
        /// <summary>
        /// Whether this note should be drawn as readable on the screen
        /// </summary>
        public bool DrawNote;

        /// <summary>
        /// Whether this note changes to next level
        /// </summary>
        public bool ChangesLevel;

        /// <summary>
        /// The type of note
        /// </summary>
        public NoteType Type;

        /// <summary>
        /// Which level this note is found in
        /// </summary>
        public int Level;

        private Texture2D worldSprite;
        private Texture2D readableSprite;

        private Transform worldTransform;
        private Transform readableTransform;
        private SpriteFont noteFont;

        /// <summary>
        /// Creates a new instance of the Note class
        /// </summary>
        /// <param name="changesLevel">Whether this note changes to next level</param>
        /// <param name="type">The type of note</param>
        /// <param name="spriteName">The name of the sprite to use when note is readable</param>
        /// <param name="transform">The Transform for this note</param>
        /// <param name="level">Which level this note is found in</param>
        public Note(bool changesLevel, NoteType type, string spriteName, Transform transform, int level) : base(transform)
        {
            this.worldSprite = GameWorld.ContentManager.Load<Texture2D>("documentopen");
            this.noteFont = GameWorld.ContentManager.Load<SpriteFont>("ExampleFont");

            this.readableSprite = GameWorld.ContentManager.Load<Texture2D>(spriteName);

            base.Sprite = worldSprite;

            this.worldTransform = transform;

            // Place this note when readable in the dead center of the screen
            this.readableTransform = new Transform(new Vector2((1920 * 0.5f) - (this.readableSprite.Width * 0.5f), (1080 * 0.5f) - (this.readableSprite.Height * 0.5f)), 0f);

            this.DrawNote = false;
            this.ChangesLevel = changesLevel;
            this.Type = type;
            this.Level = level;
        }

        /// <summary>
        /// Handles the update logic for this note
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            if (DrawNote)
            {
                if(Keyboard.GetState().IsKeyDown(Keys.Enter)||GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.Y))
                {
                    GameWorld.PickedUpNote(this);
                    GameWorld.RemoveGameObject(this);
                }
            }
        }

        /// <summary>
        /// Draws this note on the screen
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch from GameWorld</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            // Draw note as readable if applicable
            if (DrawNote)
            {
                spriteBatch.DrawString(noteFont, "Press Triangle to continue", new Vector2(base.Hitbox.Left + 30, base.Hitbox.Bottom + 30), Color.White, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 1);
            }
        }

        /// <summary>
        /// Handles collision logic with other GameObject
        /// </summary>
        /// <param name="other"></param>
        public override void OnCollision(GameObject other)
        {
            if (!DrawNote)
            {
                // Change to readable of the other GameObject is a Player
                if (other is Player)
                {
                    DrawNote = true;

                    base.Sprite = readableSprite;
                    base.Transform = readableTransform;
                }
            }
        }
    }
}
