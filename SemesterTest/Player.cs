﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SemesterTest
{
    /// <summary>
    /// Player class controls all methods and other functions that involve the player
    /// </summary>
    public class Player : AnimatedGameObject
    {
        /// <summary>
        /// Modifiers for player movement speed, gravity and jumpspeed
        /// </summary>
        float movementSpeed, gravity, jumpSpeed;

        /// <summary>
        /// Timer for player imortality
        /// </summary>
        Timer immortalityTimer = new Timer();
        /// <summary>
        /// Player is immortal for an x amount of time
        /// </summary>
        int immortalityDelay;
        /// <summary>
        /// Checks if player can take damage or not
        /// </summary>
        bool immortal;
        /// <summary>
        /// Timer to keep track of player mana regeneration
        /// </summary>
        Timer manaRegenTimer = new Timer();
        /// <summary>
        /// Delay for x amount of time before start regenerating mana again
        /// </summary>
        int manaRegenDelay;
        /// <summary>
        /// Timer for player shooting to avoid spamming shoot
        /// </summary>
        Timer shootTimer = new Timer();
        /// <summary>
        /// Delay x amount of time until player can shoot again
        /// </summary>
        int shootTimerDelay;
        /// <summary>
        /// Checks if player can shoot
        /// </summary>
        bool shootDisabled;
        /// <summary>
        /// Checks if player is looking left or right
        /// </summary>
        bool shootLeft;
        /// <summary>
        /// Timer to keep track of how long it last was the player jumped
        /// </summary>
        Timer jumpTimer = new Timer();
        /// <summary>
        /// Delay for when player is able to jump
        /// </summary>
        int jumpTimerDelay;
        /// <summary>
        /// Checks if player meets requirements to jump again
        /// </summary>
        bool jumpDisabled;
        /// <summary>
        /// Button needs to be pressed and released in order to jump once
        /// </summary>
        bool jumpButtonReleased;

        /// <summary>
        /// Player health points
        /// </summary>
        int healthPoints;
        /// <summary>
        /// Player mana points
        /// </summary>
        int manaPoints;
        /// <summary>
        /// Player max mana points
        /// </summary>
        int manaPointsMax;
        /// <summary>
        /// Player fireball mana cost
        /// </summary>
        int manaCost;
        /// <summary>
        /// Player line of sight for amount of tiles player can see
        /// </summary>
        public int LineOfSight;
        /// <summary>
        /// Keeping track of which projectile is in use
        /// </summary>
        Projectile CurrentProjectile;
        /// <summary>
        /// Player walk sound
        /// </summary>
        SoundEffectInstance playerWalkSound = GameWorld.ContentManager.Load<SoundEffect>("Player_walk_sound").CreateInstance();
        /// <summary>
        /// Fireball shooting sound
        /// </summary>
        SoundEffectInstance fireballSound = GameWorld.ContentManager.Load<SoundEffect>("Fireball_sound").CreateInstance();
        /// <summary>
        /// Player takes damage sound
        /// </summary>
        SoundEffectInstance playerHitSound = GameWorld.ContentManager.Load<SoundEffect>("Player_hit_sound").CreateInstance();

        /// <summary>
        /// Player health points property
        /// </summary>
        public int HealthPoints
        {
            get => healthPoints;
        }
        /// <summary>
        /// Player mana points property
        /// </summary>
        public int ManaPoints
        {
            get => manaPoints;

        }

        public Player(List<string> spriteNames, Dictionary<string, int> animationFrames, float startingFPS, Transform Transform) 
            : base(spriteNames, animationFrames, startingFPS, Transform)
        {
            this.movementSpeed = 75;
            this.jumpSpeed = 5f;
            this.healthPoints = 5;
            this.manaPoints = 0;
            this.manaPointsMax = 5;
            this.manaCost = 2;

            this.LineOfSight = 5;

            this.immortalityDelay = 100;
            this.immortal = false;
            this.immortalityTimer.Interval = immortalityDelay;
            this.immortalityTimer.Elapsed += ImmortalityTimerHandler;
            this.immortalityTimer.AutoReset = false;

            this.manaRegenDelay = 800;
            this.manaRegenTimer.Interval = manaRegenDelay;
            this.manaRegenTimer.Elapsed += ManaRegenTimerHandler;
            this.manaRegenTimer.Start();

            this.shootTimerDelay = 200;
            this.shootDisabled = false;
            this.shootTimer.Interval = shootTimerDelay;
            this.shootTimer.Elapsed += ShootTimerHandler;
            this.shootTimer.AutoReset = false;

            this.shootLeft = false;

            this.jumpTimerDelay = 1;
            this.jumpDisabled = false;
            this.jumpButtonReleased = true;
            this.jumpTimer.Interval = jumpTimerDelay;
            this.jumpTimer.Elapsed += JumpTimerHandler;
            this.jumpTimer.AutoReset = false;



        }
        /// <summary>
        /// Player update
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {

            Move(gameTime);

            base.Transform.Position += base.Transform.Velocity;

            //Timer
            Jump();
            
            

                base.Transform.Velocity.Y += 0.2f * 1;

            Shoot();


            base.Update(gameTime);
            
        }
        /// <summary>
        /// Player collision with GameObject
        /// </summary>
        /// <param name="other"></param>
        public override void OnCollision(GameObject other)
        {
            if (other is Note)
            {
                //GameWorld.GameOver(true);
            }
        }
        /// <summary>
        /// Player hitbox
        /// </summary>
        public override Rectangle Hitbox
        {
            get { return new Rectangle((int)Transform.Position.X+1, (int)Transform.Position.Y, Sprite.Width/Frames.Length-3, Sprite.Height); }
        }
        /// <summary>
        /// Player collision with Tile
        /// </summary>
        /// <param name="tile"></param>
        public override void OnCollision(Tile tile)
        { 
            //Player
            float myRight = this.Hitbox.Right;
            float myLeft = this.Hitbox.Left;
            float myTop = this.Hitbox.Top;
            float myBottom = this.Hitbox.Bottom;

            //Tile
            float otherRight = tile.Hitbox.Right;
            float otherLeft = tile.Hitbox.Left;
            float otherTop = tile.Hitbox.Top;
            float otherBottom = tile.Hitbox.Bottom;

            float dX = 0;
            float dY = 0;

            //Touching right side of player
            if (myRight + this.Transform.Velocity.X > otherLeft && myLeft < otherLeft && myBottom > otherTop && myTop < otherBottom)
            {
                dX = otherLeft - myRight;
                if (this.Transform.Velocity.Y < 0)
                {
                    this.Transform.Velocity.Y += 0.01f;
                }
            }

            //Touching left side of player
            if (myLeft + this.Transform.Velocity.X < otherRight && myRight > otherRight && myBottom > otherTop && myTop < otherBottom)
            {
                dX = otherRight - myLeft;
                if (this.Transform.Velocity.Y < 0)
                {
                    this.Transform.Velocity.Y += 0.01f;
                }
            }

            //Touching top side of player
            if (myTop + this.Transform.Velocity.X < otherBottom && myBottom > otherBottom && myRight > otherLeft && myLeft < otherRight)
            {
                dY = otherBottom - myTop;
            }

            //Touching bottom side of player 
            if (myBottom + this.Transform.Velocity.X > myTop && myTop < otherTop && myRight > tile.Hitbox.Left && myLeft < otherRight)
            {
                dY = otherTop - myBottom;
            }

            // If less collision on Y
            if (Math.Abs(dY) > Math.Abs(dX) && dX != 0 || Math.Abs(dX) > Math.Abs(dY) && dY == 0)
            {
                this.Transform.Position.X += dX;
            }

            //If collision on X or equal
            else if (Math.Abs(dX) > Math.Abs(dY) && dY != 0 || Math.Abs(dY) > Math.Abs(dX) && dX == 0)
            {
                this.Transform.Position.Y += dY;

                // If colliding on bottom IE touching ground
                if (dY < 0)
                {
                    this.Transform.Velocity.Y = 0f;

                    if (jumpDisabled)
                    {
                        this.jumpTimer.Start();
                    }
                }
                else if(dY > 0)
                {
                    this.Transform.Velocity.Y = .1f;
                }
            }
        }
        /// <summary>
        /// Player take damage method
        /// </summary>
        /// <param name="dmg"></param>
        public void TakeDamage(float dmg)
        {
            if (! immortal && healthPoints > 0)
            {
                healthPoints -= (int)dmg;
                playerHitSound.Play();

                if (healthPoints > 0)
                {
                    this.immortal = true;
                    this.immortalityTimer.Start();
                }
                else
                {
                    GameWorld.GameOver(false);
                }
            }
        }
        /// <summary>
        /// Player shooting method
        /// </summary>
        public void Shoot()
        {
            if(! shootDisabled)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.C) && manaPoints >= manaCost || GamePad.GetState(PlayerIndex.One).Buttons.B == ButtonState.Pressed && manaPoints >= manaCost)
                {
                    ChangeAnimation("Player_attack", false);
                    shootDisabled = true;
                    shootTimer.Start();
                    fireballSound.Play();

                    manaPoints -= manaCost;
                    if (shootLeft)
                    {
                        GameWorld.AddGameObject(new Projectile(-5, 1, GameWorld.projectileName, GameWorld.fireballAnimations, 5f, Transform));
                    }
                    else
                    {
                        GameWorld.AddGameObject(new Projectile(5, 1, GameWorld.projectileName, GameWorld.fireballAnimations, 5f, Transform));
                    }
                }
            }           
        }

        /// <summary>
        /// Move is a method within the player class to control all the player movement
        /// </summary>
        public void Move(GameTime gameTime)
        {
            
            //Left movement
            if (Keyboard.GetState().IsKeyDown(Keys.A) || Keyboard.GetState().IsKeyDown(Keys.Left) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X < 0)
            {
                ChangeAnimation("Player_walk", true);
                flipAnimation = true;
                base.Transform.Position.X -= 1 * (float)(movementSpeed * gameTime.ElapsedGameTime.TotalSeconds);
                shootLeft = true;
                playerWalkSound.Play();
                
            }

            //Right movement
            else if (Keyboard.GetState().IsKeyDown(Keys.D) || Keyboard.GetState().IsKeyDown(Keys.Right) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X > 0)
            {
                ChangeAnimation("Player_walk", true);
                flipAnimation = false;
                base.Transform.Position.X += 1 * (float)(movementSpeed * gameTime.ElapsedGameTime.TotalSeconds);
                shootLeft = false;
                playerWalkSound.Play();
            }
            else
            {
                ChangeAnimation("Player_idle", true);
                playerWalkSound.Stop();
            }
        }
        /// <summary>
        /// Jump method controls the player vertical movement
        /// </summary>
        public void Jump()
        {
            if (! jumpDisabled && Keyboard.GetState().IsKeyDown(Keys.Space) || !jumpDisabled && Keyboard.GetState().IsKeyDown(Keys.Up) || !jumpDisabled && GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
            {
                //Jump and gravity
                if ( base.Transform.Velocity.Y == 0 && this.jumpButtonReleased || base.Transform.Velocity.Y == 0 && this.jumpButtonReleased)
                {
                    //Jump height and smoother
                    base.Transform.Position.Y -= 5f;
                    base.Transform.Velocity.Y = -jumpSpeed;

                    this.jumpDisabled = true;
                    this.jumpButtonReleased = false;
                }
            }
            else if (!Keyboard.GetState().IsKeyDown(Keys.Space) && !Keyboard.GetState().IsKeyDown(Keys.Up) && GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Released)
            {
                this.jumpButtonReleased = true;
            }
        }


        private void ImmortalityTimerHandler(object sender, EventArgs e)
        {
            this.immortal = false;
        }

        private void ManaRegenTimerHandler(object sender, EventArgs e)
        {
            if(this.manaPoints < this.manaPointsMax)
            {
                this.manaPoints += 1;
            }
        }
        
        private void ShootTimerHandler (object sender, EventArgs e)
        {
            this.shootDisabled = false;
        }

        private void JumpTimerHandler (object sender, EventArgs e)
        {
            this.jumpDisabled = false;
        }
    }
}
