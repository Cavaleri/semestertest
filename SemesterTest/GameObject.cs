﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// GameObject keeps track of all the gameobjects in the GameWorld
    /// </summary>
    public class GameObject
    {
        /// <summary>
        /// The soundeffect currenly being played
        /// </summary>
        protected SoundEffectInstance soundEffect;

        /// <summary>
        /// The radius where this sound can be heard from Player perspective
        /// </summary>
        protected float soundRadius;

        /// <summary>
        /// contains the objects sprite
        /// </summary>
        public Texture2D Sprite;
        /// <summary>
        /// a string to hold the name of the sprite
        /// </summary>
        public string spriteName;
        /// <summary>
        /// the center of the sprite kept in a vector2
        /// </summary>
        public Vector2 spriteCenter;
        /// <summary>
        /// Transform class holds positions and such for the object
        /// </summary>
        public Transform Transform;
        /// <summary>
        /// Makes a rectangle that will be made into the objects hitbox
        /// </summary>
        public virtual Rectangle Hitbox
        {
            get { return new Rectangle((int)Transform.Position.X, (int)Transform.Position.Y, Sprite.Width, Sprite.Height); }
        }
        /// <summary>
        /// Constructor for our GameObject
        /// </summary>
        /// <param name="Sprite">The sprite for the specific object</param>
        /// <param name="Transform">All positions and such is held in here</param>
        public GameObject(string spriteName, Transform Transform)
        {
            this.Sprite = GameWorld.ContentManager.Load<Texture2D>(spriteName);
            this.Transform = Transform;
            spriteCenter = new Vector2(Sprite.Width * 0.5f, Sprite.Height * 0.5f);
        }
        public GameObject(Transform transform)
        {
            this.Transform = transform;

        }
        /// <summary>
        /// Update for gameobjects
        /// </summary>
        /// <param name="gameTime">Time in game</param>
        public virtual void Update(GameTime gameTime)
        {

        }
        /// <summary>
        /// Draw for out gameobjects
        /// </summary>
        /// <param name="gameTime">Finds picture of said sprite</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, new Vector2(Transform.Position.X, Transform.Position.Y - 1), null, Color.White, 0f, spriteCenter, 1f, SpriteEffects.None, 1f);
        }
        /// <summary>
        /// Checks if an object is colliding with another object
        /// </summary>
        /// <param name="other">other gameobjects</param>
        /// <returns></returns>
        public virtual bool CheckCollision(GameObject other)
        {
            return Hitbox.Intersects(other.Hitbox);
        }
        /// <summary>
        /// checks the collision on tiles
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public virtual bool CheckCollision(Tile platform)
        {
            if (platform.Collideable)
            {
                return this.Hitbox.Intersects(platform.Hitbox);
            }

            return false;
        }

        /// <summary>
        /// What to do on collision with other objects
        /// </summary>
        /// <param name="other">other objects</param>
        public virtual void OnCollision(GameObject other)
        {

        }
        /// <summary>
        /// What happens on collision
        /// </summary>
        /// <param name="other"></param>
        public virtual void OnCollision(Tile other)
        {

        }

        /// <summary>
        /// Check whether this sound can be heard from the Players perspective within the sound radius
        /// </summary>
        /// <param name="playerHitbox"></param>
        public void CheckForSoundPlayability(Rectangle playerHitbox)
        {
            if (soundEffect != null)
            {
                Point playerCenter = playerHitbox.Center;
                Point myCenter = this.Hitbox.Center;

                float dX = myCenter.X - playerCenter.X;
                float dY = myCenter.Y - playerCenter.Y;

                // Calculate the distance between each point
                double distanceSquared = Math.Pow(playerCenter.X - myCenter.X, 2) + Math.Pow(playerCenter.Y - myCenter.Y, 2);
                float distance = (float)Math.Sqrt(distanceSquared);

                if (distance < soundRadius)
                {
                    CalculateSoundEffect(distance, dX);
                }
                else
                {
                    soundEffect.Stop();
                }
            }
        }

        /// <summary>
        /// Calculate the volume and pan for the sound effect
        /// </summary>
        /// <param name="distance">Distance from Player</param>
        /// <param name="distanceX">Distance on X-axis only</param>
        private void CalculateSoundEffect(float distance, float distanceX)
        {
            float distanceAsPercentage = distance / soundRadius;

            // Set volume based on percentage
            float volume = Math.Abs(distanceAsPercentage - 1);

            float pan = distanceX / soundRadius;

            soundEffect.Volume = volume;
            soundEffect.Pan = pan;
            if (soundEffect.State == SoundState.Stopped)
            {
                soundEffect.Play();
            }
        }
    }
}
