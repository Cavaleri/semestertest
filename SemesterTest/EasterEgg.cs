﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace SemesterTest
{
    /// <summary>
    /// The EasterEgg class holds the information for out easter egg room
    /// </summary>
    public class EasterEgg : GameObject
    {
        public Rectangle ChristmasRectangle;
        private bool wasCollided;

        public EasterEgg(string spriteName, Transform Transform) : base(spriteName, Transform)
        {
            this.ChristmasRectangle = new Rectangle(16, 16, 16 * 16, 7 * 16);
            this.wasCollided = false;
        }

        public bool CheckCollisionWithChristmas(GameObject player)
        {
            if (this.ChristmasRectangle.Intersects(player.Hitbox))
            {
                return true;
            }
            else if (wasCollided)
            {
                wasCollided = false;
                GameWorld.ChangeBacktrack(false);
            }
            return false;
            
        }

        public void OnCollisionWithChristmas(GameObject player)
        {
            if (!wasCollided)
            {
                GameWorld.ChangeBacktrack(true);
            }

            // Set collided
            wasCollided = true;
        }
    }
}
