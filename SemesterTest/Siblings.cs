﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterTest
{
    /// <summary>
    /// An instance of the Non Playable Characters
    /// </summary>
    public class Siblings : GameObject
    {
        /// <summary>
        /// Creates a new NPC
        /// </summary>
        /// <param name="spriteName">Name of the sprite to draw</param>
        /// <param name="Transform">Transform for this NPC</param>
        public Siblings(string spriteName, Transform Transform) : base(spriteName, Transform)
        {

        }
        /// <summary>
        /// Handles collision logic
        /// </summary>
        /// <param name="other"></param>
        public override void OnCollision(GameObject other)
        {
            if (other is Player)
            {
                GameWorld.GameOver(true);
            }
        }
        /// <summary>
        /// Checks for collision with the Tilemap
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public override bool CheckCollision(Tile platform)
        {
            // Disable collision checks
            return false;
        }
    }
}
