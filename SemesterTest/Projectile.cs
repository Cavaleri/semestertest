﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SemesterTest
{
    /// <summary>
    /// The Projectile class keeps methods and value for the game projectiles
    /// </summary>
    public class Projectile : AnimatedGameObject
    {
        /// <summary>
        /// Speed for projectile
        /// </summary>
        public float Speed;
        /// <summary>
        /// Damage for projectile
        /// </summary>
        public int Damage;
        /// <summary>
        /// Lifespan of fireball until it respawns
        /// </summary>
        private float Lifespan = 2f;
        /// <summary>
        /// Timer for projectile
        /// </summary>
        private float Timer;

        public int LineOfSight;


        /// <summary>
        /// For adjustment of damage, depending on what projectiles that are being shot
        /// </summary>
        /// <returns></returns>
        public int GetDamage()
        {
            return Damage;
        }
        /// <summary>
        /// Damage property
        /// </summary>
        /// <param name="value"></param>
        public void SetDamage(int value)
        {
            Damage = value;
        }

        public Projectile(float speed, int damage, List<string> spriteNames, Dictionary<string, int> animationFrames, float startingFPS, Transform Transform) :
            base(spriteNames, animationFrames, startingFPS, Transform)
        {

            this.Speed = speed;
            SetDamage(damage);

            this.LineOfSight = 2;
        }
        /// <summary>
        /// Projectile hitbox
        /// </summary>
        public override Rectangle Hitbox
        {
            get { return new Rectangle((int)Transform.Position.X, (int)Transform.Position.Y, Sprite.Width / Frames.Length, Sprite.Height); }

        }
        /// <summary>
        /// Checks collisions with walls from all sides and removes itself on contact
        /// </summary>
        /// <param name="tile"></param>
        public override void OnCollision(Tile tile)
        {
            if (this.Hitbox.Right + this.Transform.Position.X > tile.Hitbox.Left &&
                this.Hitbox.Left < tile.Hitbox.Left &&
                this.Hitbox.Bottom > tile.Hitbox.Top &&
                this.Hitbox.Top < tile.Hitbox.Bottom)
            {
                GameWorld.RemoveGameObject(this);
            }

            if (this.Hitbox.Left + this.Transform.Position.X < tile.Hitbox.Right &&
                       this.Hitbox.Right > tile.Hitbox.Right &&
                       this.Hitbox.Bottom > tile.Hitbox.Top &&
                       this.Hitbox.Top < tile.Hitbox.Bottom)
            {
                GameWorld.RemoveGameObject(this);
            }

            if (this.Hitbox.Bottom + this.Transform.Position.X > tile.Hitbox.Top &&
                   this.Hitbox.Top < tile.Hitbox.Top &&
                   this.Hitbox.Right > tile.Hitbox.Left &&
                   this.Hitbox.Left < tile.Hitbox.Right)
            {
                GameWorld.RemoveGameObject(this);
            }

            if (this.Hitbox.Top + this.Transform.Position.X < tile.Hitbox.Bottom &&
                   this.Hitbox.Bottom > tile.Hitbox.Bottom &&
                   this.Hitbox.Right > tile.Hitbox.Left &&
                   this.Hitbox.Left < tile.Hitbox.Right)
            {
                GameWorld.RemoveGameObject(this);
            }
        }

        
        /// <summary>
        /// Projectile update loop
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {            
            GetDamage();
            Movement();
            Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (Timer >= Lifespan)
            {
                GameWorld.RemoveGameObject(this);
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Projectile collision with GameObjects
        /// </summary>
        /// <param name="other"></param>
        public override void OnCollision(GameObject other)
        {
            //Attacks when colliding with Enemy and removes itself
            if (other is Enemy)
            {
                Attack((Enemy)other);
                GameWorld.RemoveGameObject(this);
            }
        }
        // Reference to make collision -> dmg work with the general kollision above ^
        private void Attack(Enemy enemy)
        {
            enemy.TakeDamage(Damage);
        }
        /// <summary>
        /// Projectile/Fireball movement method
        /// </summary>
        public void Movement()
        {
            Transform.Position.X += Speed;

            if (Speed < 0)
            {
                flipAnimation = true;
            }
            else
            {
                flipAnimation = false;
            }
        }
    }
}
